# Instrument a Rust Lambda Function with Logging and Tracing

## 1. Select an USED Rsut Lambda Project
Here I choose the 'most_frequent_number' project which can return the most frequent number in an array when input a JSON file. More deatiled info, please click [here](https://gitlab.com/estrellas939/cargo_lambda_with_aws_api) 

## 2. Add Config

In the `Cargo.toml` file, add new dependencies:
```
[dependencies]
lambda_http = "0.9.2"
lambda_runtime = "0.10.0"
tokio = { version = "1", features = ["macros"] }
tracing = { version = "0.1", features = ["log"] }
tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
log = "0.4"
simple_logger = "4.3.3"
```
Here `log` and `simple_logger` are dependencies used for logging and tracing.

## 3. Modify Code
In the `main.rs`, we need modify some lines of code to satisfy the logging function:

```#[tokio::main]
async fn main() -> Result<(), Error> {
    simple_logger::init_with_level(log::Level::Info)?;
    run(service_fn(function_handler)).await?;
    Ok(())
}
```
For more detailed modification, please check `main.rs`.

## 4. Local Test
After bulding the code, we need to do a local test to see the functionality.
In the terminal, type:
`cargo lambda watch`
And we should see the following output:
```
INFO invoke server listening on [::]:9000
 INFO starting lambda function function="_" manifest="Cargo.toml"
   Compiling colored v2.1.0
   Compiling simple_logger v4.3.3
   Compiling most_frequent_number v0.1.0 (/Users/wanghaotian/week6proj)
    Finished dev [unoptimized + debuginfo] target(s) in 6.74s
     Running `target/debug/most_frequent_number`
```
Open anothe terminal and type:
```
cargo lambda invoke --data-file event.json
```
We should see the following output from the new terminal:
```
{"statusCode":200,"headers":{"content-type":"application/json"},"multiValueHeaders":{"content-type":["application/json"]},"body":"{\"message\":\"no such value due to same frequent\"}","isBase64Encoded":false}
```
From the original terminal, we can also see the logging output:
```
2024-02-28T21:29:17.566Z INFO  [lambda_runtime::types] Lambda runtime invoke; requestId="23707575-a596-47f1-96d2-e4e74955c143" xrayTraceId="Root=1-65dfa5ad-8f3c145b55de76ee6701698c;Parent=df5a3f7f5b0ff158;Sampled=1"
2024-02-28T21:29:17.567Z INFO  [most_frequent_number] Parsed numbers successfully.
```

## 5. Lambda Binary File Build
Use `cargo lambda build --release` to compile Lambda function to a binary file for deployment.
```
Compiling most_frequent_number v0.1.0 (/Users/wanghaotian/week6proj)
    Finished release [optimized] target(s) in 1m 07s
```

## 6. Deploy Rsut Function to AWS Lambda
Use `cargo lambda deploy` to deploy the binary file to AWS Lambda. And we should see the following output:
```
🔍 function arn: arn:aws:lambda:us-east-1:568937614068:function:most_frequent_number:2
```

## 7. Create a New Role in AWS IAM
Log in the AWS account, and create a new role under AWS IAM panel. Remember to add permissions releated to `CloudWatch` and `X-ray` to that role. So that this role can have access to logging and tracing via X-ray.
![screenshot of iam role](https://gitlab.com/estrellas939/week6proj/-/raw/main/pic/iam-role.png?inline=false)

## 8. Lambda Function Configuration
Back to the AWS Lambda panel, click the lambda function 'most_frequent_number'.
Under the `Configuration` sub-panel, edit `Execution role` and change to the new role we created in step 7.
![screenshot of Execution role](https://gitlab.com/estrellas939/week6proj/-/raw/main/pic/excu-role.png?inline=false)
Also we need to enable `CloudWatch log group` as `Logging configuration` from `Monitoring and operations tools` on the left navigation bar.
![screenshot of logging config](https://gitlab.com/estrellas939/week6proj/-/raw/main/pic/log-config.png?inline=false)

## 9. Test
Under the `Test` sub-panel, click `Test`. We can see the lambda function pass the test case successfully.
![screenshot of test](https://gitlab.com/estrellas939/week6proj/-/raw/main/pic/test.png?inline=false)

## 10. CloudWatch
Go to AWS CloudWatch panel, we can find 'most_frequent_number' log group. Under `Log stream` sub-panel, we can see the last activity record.
![screenshot of log stream](https://gitlab.com/estrellas939/week6proj/-/raw/main/pic/log-group.png?inline=false)
![log detail](https://gitlab.com/estrellas939/week6proj/-/raw/main/pic/log-detail.png?inline=false)

## 11. X-Ray logging
X-Ray console now has integrated with CloudWatch. We can easily found it from the left navigation bar. We can also track the last activity via X-Ray.
![screenshot of xray](https://gitlab.com/estrellas939/week6proj/-/raw/main/pic/xray.png?inline=false)
![xray detail](https://gitlab.com/estrellas939/week6proj/-/raw/main/pic/trace-detail.png?inline=false)
